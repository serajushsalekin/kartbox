from django.db import models
from addresses.models import Address
from carts.models import Cart
from billing.models import BillingProfile
from .utils import unique_id_generator
from django.db.models.signals import pre_save, post_save


STATUS_CHOICES = (
    ('created', 'Created'),
    ('paid', 'Paid'),
    ('shipped', 'Shipped'),
    ('refunded', 'Refunded'),
)


class OrderManager(models.Manager):
    def new_or_get(self, billing_profile, cart):
        created = False
        qs = self.get_queryset().filter(
            billing_profile=billing_profile,
            cart=cart,
            status='created',
            active=True)
        if qs.count() == 1:
            order = qs.first()
        else:
            order = self.model.objects.create(billing_profile=billing_profile, cart=cart)
            created = True
        return order, created


class Order(models.Model):
    billing_profile = models.ForeignKey(BillingProfile, null=True, blank=True, on_delete=models.CASCADE)
    order_id = models.CharField(max_length=255, blank=True)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    shipping_address = models.ForeignKey(Address, null=True, blank=True, on_delete=models.CASCADE)
    status = models.CharField(max_length=255, default='created', choices=STATUS_CHOICES)
    active = models.BooleanField(default=True)
    total = models.DecimalField(default=0.00, decimal_places=2, max_digits=10)

    objects = OrderManager()

    def __str__(self):
        return self.order_id

    def update_total(self):
        cart_total = self.cart.total
        self.total = cart_total
        self.save()
        return cart_total

    def checkout_done(self):
        billing_profile = self.billing_profile
        shipping_address = self.shipping_address
        total = self.total
        if billing_profile and shipping_address and total > 0:
            return True
        return False

    def mark_done(self):
        if self.checkout_done():
            self.status = 'paid'
            self.save()
        return self.status


def create_order_id(sender, instance, *args, **kwargs):
    if not instance.order_id:
        instance.order_id = unique_id_generator(instance)
    qs = Order.objects.filter(cart=instance.cart).exclude(billing_profile=instance.billing_profile)
    if qs.exists:
        qs.update(active=False)


pre_save.connect(create_order_id, sender=Order)


def cart_total_update(sender, instance, created, *args, **kwargs):
    if not created:
        cart_id = instance.id
        qs = Order.objects.filter(cart__id=cart_id)
        if qs.count() == 1:
            order_obj = qs.first()
            order_obj.update_total()


post_save.connect(cart_total_update, sender=Cart)


def order_update(sender, instance, created, *args, **kwargs):
    if created:
        instance.update_total()


post_save.connect(order_update, sender=Order)
