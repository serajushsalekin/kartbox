from django.conf.urls import url
from .views import Signup, Login
from django.contrib.auth.views import LogoutView
from django.urls import reverse_lazy

app_name = 'account'

urlpatterns = [
    url(r'^signup/$', Signup.as_view(), name='signup'),
    url(r'^signin/$', Login.as_view(), name='signin'),
    url(r'^logout/$', LogoutView.as_view(next_page=reverse_lazy('home')), name='logout'),
]