from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.http import is_safe_url
from django.views.generic import FormView, CreateView, TemplateView
from .forms import SignupForm, LoginForm


class Index(TemplateView):
    template_name = 'master3.html'


class Signup(CreateView):
    template_name = 'account/signup.html'
    form_class = SignupForm
    success_url = reverse_lazy('account:signin')


class Login(FormView):
    template_name = 'account/signin.html'
    form_class = LoginForm

    def form_valid(self, form):
        request = self.request
        next_ = request.GET.get('next')
        next_post = request.POST.get('next', '/')
        redirect_path = next_ or next_post
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        user = authenticate(request, username=email, password=password)
        if user is not None:
            login(request, user)
            if is_safe_url(redirect_path, request.get_host()):
                return HttpResponseRedirect(redirect_path)
            else:
                return redirect('home')
        return super(Login, self).form_invalid(form)

