from django.db import models
from django.conf import settings
from django.db.models.signals import post_save

User = settings.AUTH_USER_MODEL


class BillingManager(models.Manager):
    def new_or_get(self, request):
        usr = request.user
        email = usr.email
        obj = None
        created = False
        if usr.is_authenticated:
            obj, created = self.model.objects.get_or_create(user=usr, email=email)

        return obj, created


class BillingProfile(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
    email = models.EmailField()
    active = models.BooleanField(default=True)

    objects = BillingManager()

    def __str__(self):
        return self.email


def user_n_billing_created(sender, instance, created, *args, **kwargs):
    if created and instance.email:
        BillingProfile.objects.get_or_create(user=instance, email=instance.email)


post_save.connect(user_n_billing_created, sender=User)
