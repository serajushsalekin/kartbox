from .models import Cart


def cart_count(request):
    cart, created = Cart.objects.new_or_get(request)
    count = cart.cartitem_set.count()
    return {'cart_count': count}
