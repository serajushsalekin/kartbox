from django.conf.urls import url
from .views import add_cart, cart_detail, cart_remove, remove, cart_update, shippment_cost

app_name = 'carts'


urlpatterns = [
    url(r'^$', cart_detail, name='details'),
    url(r'^cart_add/(?P<product_id>[0-9]+)/$', add_cart, name='add'),
    url(r'^plus/(?P<product_id>[0-9]+)/$', cart_update, name='cart_plus'),
    url(r'^minus/(?P<product_id>[0-9]+)/$', cart_remove, name='cart_minus'),
    url(r'^remove/(?P<id>\d+)/$', remove, name='delete'),
    url(r'^shiping/$', shippment_cost, name='ship_cost'),
]

#
