# Generated by Django 2.1.4 on 2019-01-07 15:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0004_auto_20190103_1213'),
        ('carts', '0003_auto_20190107_1527'),
    ]

    operations = [
        migrations.AddField(
            model_name='cart',
            name='products',
            field=models.ManyToManyField(blank=True, null=True, to='products.Product'),
        ),
    ]
