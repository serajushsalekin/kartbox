from django.contrib import admin
from .models import Cart, CartItem, ShippingMethod


class CartInline(admin.StackedInline):
    model = CartItem


class CartAdmin(admin.ModelAdmin):
    inlines = [CartInline]


admin.site.register(Cart, CartAdmin)
admin.site.register(ShippingMethod)



