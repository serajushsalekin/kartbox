from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from products.models import Product
from .models import Cart, CartItem, ShippingMethod


def cart_detail(request):
    cart, created = Cart.objects.new_or_get(request)
    ship_obj = ShippingMethod.objects.all()
    if not created:
        new_total = 0
        for item in cart.cartitem_set.all():
            total = float(item.product.price) * item.quantity
            new_total += total
        cart.total = new_total
        cart.save()

    context = {
        'cart': cart,
        'ship_obj': ship_obj,
    }
    return render(request, 'carts/cart_details.html', context)


def shippment_cost(request):
    cart, created = Cart.objects.new_or_get(request)
    try:
        ship_in = request.POST.get('method', None)
    except:
        raise ValueError('Mara khawa sara! :(')
    cart_ship = ShippingMethod.objects.get(id=int(ship_in))
    if cart_ship:
        cart.shipping_method = cart_ship
        cart.save()
    return HttpResponseRedirect(reverse('carts:details'))


def add_cart(request, product_id):
    cart, created = Cart.objects.new_or_get(request)
    product = get_object_or_404(Product, id=product_id)
    try:
        quantity = request.POST['qty']
        qty = True
    except:
        quantity = None
        qty = False

    cart_item, create = CartItem.objects.get_or_create(cart=cart, product=product)
    if cart_item:
        cart_item.quantity += int(quantity)
    else:
        cart_item.quantity = int(quantity)
    cart_item.save()
    # return HttpResponseRedirect(reverse('carts:details'))
    return redirect(request.META['HTTP_REFERER'])


def cart_update(request, product_id):
    cart, created = Cart.objects.new_or_get(request)
    product = get_object_or_404(Product, id=product_id)
    try:
        quantity = request.POST['qty']
        qty = True
    except:
        quantity = None
        qty = False

    cart_item, create = CartItem.objects.get_or_create(cart=cart, product=product)
    if cart_item:
        cart_item.quantity += 1
    else:
        cart_item.quantity = int(quantity)
    cart_item.save()
    # return HttpResponseRedirect(reverse('carts:details'))
    return redirect(request.META['HTTP_REFERER'])


def cart_remove(request, product_id):
    cart, created = Cart.objects.new_or_get(request)
    product = get_object_or_404(Product, id=product_id)
    # cart_item, create = CartItem.objects.get_or_create(cart=cart, product=product)
    cart_item = CartItem.objects.get(cart=cart, product=product)
    print(cart_item)
    if cart_item.quantity > 1:
        cart_item.quantity -= 1
        cart_item.save()
    else:
        cart_item.delete()
    # return HttpResponseRedirect(reverse('carts:details'))
    return redirect(request.META['HTTP_REFERER'])


def remove(request, id):
    cart, created = Cart.objects.new_or_get(request)
    cart_item = CartItem.objects.get(id=id)
    cart_item.cart = None
    # cart_item.delete()
    cart_item.save()
    return HttpResponseRedirect(reverse('carts:details'))
