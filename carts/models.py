from django.db import models
from django.conf import settings
from products.models import Product
from django.db.models.signals import pre_save, post_delete, post_save

User = settings.AUTH_USER_MODEL


class CartManager(models.Manager):
    def new_or_get(self, request):
        cart_id = request.session.get('cart_id', None)
        qs = self.get_queryset().filter(id=cart_id)
        if qs.count() == 1:
            new_obj = False
            cart_obj = qs.first()
            if request.user.is_authenticated and cart_obj.user is None:
                cart_obj.user = request.user
                cart_obj.save()
        else:
            cart_obj = self.new(user=request.user)
            new_obj = True
            request.session['cart_id'] = cart_obj.id

        return cart_obj, new_obj

    def new(self, user=None):
        user_obj = None
        if user is not None:
            if user.is_authenticated:
                user_obj = user
            else:
                user_obj = None
        return self.model.objects.create(user=user_obj)


class ShippingMethod(models.Model):
    title = models.CharField(max_length=25)
    cost = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def __str__(self):
        return self.title


class Cart(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product, blank=True)
    shipping_method = models.ForeignKey(ShippingMethod, blank=True, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    active = models.BooleanField(default=True)
    total = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    objects = CartManager()

    def __str__(self):
        return f'Cart Id: {self.pk}'


class CartItem(models.Model):
    cart = models.ForeignKey(Cart, null=True, blank=True, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=0)
    total_item_price = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def __str__(self):
        return self.product.name


def get_single_total(instance, **kwargs):
        if int(instance.quantity) >= 1:
            total = float(instance.product.price) * int(instance.quantity)
            instance.total_item_price = total


pre_save.connect(get_single_total, sender=CartItem)


def user_presave(instance, **kwargs):
    if not instance.user:
        user = instance.user
        return user
    else:
        return instance.user


pre_save.connect(user_presave, sender=Cart)


def total_cost(instance, **kwargs):
    if instance.shipping_method:
        total = float(instance.total) + float(instance.shipping_method.cost)
        instance.total = total


pre_save.connect(total_cost, sender=Cart)



