from django import forms
from .models import ShippingMethod


class ShipForm(forms.Form):
    shipment = forms.ModelChoiceField(queryset=ShippingMethod.objects.all().order_by('-title'))
