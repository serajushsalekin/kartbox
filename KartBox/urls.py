from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from products.views import FeaturedProduct

urlpatterns = [
    path('admin/', admin.site.urls),
    path('account/', include('account.urls')),
    path('products/', include('products.urls')),
    path('carts/', include('carts.urls')),
    path('', FeaturedProduct.as_view(), name='home'),
    path('order/', include('checkout.urls')),
    path('address/', include('addresses.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    # urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
