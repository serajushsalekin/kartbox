from django import forms
from .models import Address
from django.forms import TextInput


class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = '__all__'
        exclude = [
            'billing_profile'
        ]
        widgets = {
            'address_line': TextInput(attrs={'class': 'form-control mb-4'}),
            'postal_code': TextInput(attrs={'class': 'form-control mb-4'}),
            'city': TextInput(attrs={'class': 'form-control mb-4'}),
            'country': TextInput(attrs={'class': 'form-control mb-4'})
        }

