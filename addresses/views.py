from django.shortcuts import redirect

from addresses.models import Address
from billing.models import BillingProfile
from .forms import AddressForm


def address_create(request):
    print('ok')
    form = AddressForm(request.POST)
    if form.is_valid():
        instance = form.save(commit=False)
        billing, created = BillingProfile.objects.new_or_get(request)
        if billing is not None:
            instance.billing_profile = billing
            instance.save()
            request.session['shipping_address_id'] = instance.id
            return redirect('order:checkout')
        else:
            return redirect('order:checkout')
    elif request.method == 'POST':
        print(request.POST)
        shipping_address = request.POST.get('shipping_address', None)
        billing, created = BillingProfile.objects.new_or_get(request)
        if shipping_address is not None:
            qs = Address.objects.filter(billing_profile=billing, id=shipping_address)
            if qs:
                request.session['shipping_address_id'] = shipping_address

        return redirect('order:checkout')

    else:
        return redirect('order:checkout')



