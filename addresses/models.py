from django.db import models
from billing.models import BillingProfile


class Address(models.Model):
    billing_profile = models.ForeignKey(BillingProfile, on_delete=models.CASCADE)
    address_line = models.CharField(max_length=100)
    postal_code = models.CharField(max_length=20)
    city = models.CharField(max_length=20)
    country = models.CharField(max_length=20)

    def __str__(self):
        return self.billing_profile.email

    def get_full_address(self):
        return f'{self.address_line},city:{self.city}'
