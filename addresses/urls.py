from django.urls import path
from .views import address_create

app_name = 'address'


urlpatterns = [
    path('', address_create, name='create'),
]
