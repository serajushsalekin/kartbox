from django.db import models
from django.shortcuts import reverse
from .utils import unique_slug_generator
from django.db.models.signals import pre_save


class ProductManager(models.Manager):
    def featured(self):
        return self.get_queryset().filter(featured=True)


class Product(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(unique=True, blank=True)
    des = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    preview = models.ImageField(upload_to='media/%Y/%m/%d', blank=False)
    featured = models.BooleanField(default=False)

    objects = ProductManager()

    def __str__(self):
        return self.name.capitalize()

    def get_absolute_url(self):
        return reverse('products:details', args=[self.slug])


class Image(models.Model):
    product = models.ForeignKey(Product, default=None, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField(upload_to='media/%Y/%m/%d', blank=True)

    def __str__(self):
        return self.product.name.capitalize()


def slugify_product_slug(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)
    # slug = slugify(instance.name)
    # exists = Product.objects.filter(slug=slug).exists()
    # if exists:
    #     slug = '%s-%s' %(slug, instance.id)
    # instance.slug = slug


pre_save.connect(slugify_product_slug, sender=Product)
