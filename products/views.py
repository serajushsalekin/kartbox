from django.views.generic import DetailView, ListView
from .models import Product


class ProductList(ListView):
    model = Product


class ProductDetails(DetailView):
    model = Product


class FeaturedProduct(ListView):
    template_name = 'master.html'
    queryset = Product.objects.featured()
