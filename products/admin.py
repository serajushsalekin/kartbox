from django.contrib import admin
from .models import Product, Image


class ProductImageInline(admin.StackedInline):
    model = Image


class ProductAdmin(admin.ModelAdmin):
    # list_display = ['name', 'slug', 'des', 'price', 'preview', 'featured']
    # prepopulated_fields = {'slug': ('name',)}
    inlines = [ProductImageInline]


admin.site.register(Product, ProductAdmin)
# admin.site.register(Image)
