from django.urls import path
from .views import ProductList, ProductDetails

app_name = 'products'


urlpatterns = [
    path('list', ProductList.as_view(), name='list'),
    path('details/<slug:slug>', ProductDetails.as_view(), name='details'),
]