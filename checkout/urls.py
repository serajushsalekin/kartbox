from django.conf.urls import url
from .views import checkout_home, checkout_finish, checkout_success

app_name = 'order'

urlpatterns = [
    url(r'^$', checkout_home, name='checkout'),
    url(r'^done/$', checkout_finish, name='checkout_done'),
    url(r'^success/$', checkout_success, name='success')
]
