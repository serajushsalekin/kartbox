from django.shortcuts import render, redirect
from addresses.models import Address
from carts.models import Cart
from orders.models import Order
from django.contrib.auth.decorators import login_required
from billing.models import BillingProfile
from addresses.forms import AddressForm


@login_required(login_url='account:signin')
def checkout_home(request):
    cart, created = Cart.objects.new_or_get(request)
    order = None
    address = AddressForm()
    shipping_address_id = request.session.get('shipping_address_id', None)
    if created:
        return redirect('carts:details')
    billing_obj, created = BillingProfile.objects.new_or_get(request)
    address_qs = None
    if billing_obj is not None:
        address_qs = Address.objects.filter(billing_profile=billing_obj)
        order, created = Order.objects.new_or_get(billing_profile=billing_obj, cart=cart)
        if shipping_address_id:
            order.shipping_address = Address.objects.get(id=shipping_address_id)
            del request.session['shipping_address_id']
            order.save()
    # if request.method == 'POST':
    #     paid = order.checkout_done()
    #     if paid:
    #         order.mark_done()
    #         del request.session['cart_id']
    #         return redirect('/success')

    context = {
        'order_obj': order,
        'billing': billing_obj,
        'address_form': address,
        'address_qs': address_qs
    }
    return render(request, 'checkout/checkout.html', context)


@login_required(login_url='account:signin')
def checkout_finish(request):
    cart, created = Cart.objects.new_or_get(request)
    billing_obj, created = BillingProfile.objects.new_or_get(request)
    order = None
    if billing_obj is not None:
        order, created = Order.objects.new_or_get(billing_profile=billing_obj, cart=cart)
    paid = order.checkout_done()
    if paid:
        order.mark_done()
        del request.session['cart_id']
        return redirect('order:success')


def checkout_success(request):
    return render(request, 'checkout/thank_you.html', {})
